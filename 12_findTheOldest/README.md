# Ejercicio 12 - Encontrar al más viejo

Dado un array de objetos que representan personas con un año de nacimiento y muerte, devuelve a la persona más vieja.

## Pistas
- Deberías devolver el objeto completo de la persona, pero las pruebas principalmente verifican que el nombre sea correcto.
- Esto se puede hacer con un par de métodos encadenados de arrays o utilizando `reduce`.
- Una de las pruebas verifica personas sin fecha de muerte... utiliza la función Date de JavaScript para obtener su edad hasta hoy.
