# Ejercicio 05 - sumAll

Implementa una función que tome 2 enteros y devuelva la suma de todos los números entre (y incluyendo) ellos:

```javascript
sumAll(1, 4) // returns the sum of 1 + 2 + 3 + 4 which is 10
```


## Pistas

Piensa en cómo harías esto en papel y luego cómo podrías traducir ese proceso a código:
- Asegúrate de prestar atención a los parámetros de la función
- Crea una variable para contener la suma final
- Recorre los números dados ([enlace](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Bucles_e_iteracion))
- En cada iteración, agrega el número a la suma
- Devuelve la suma después de terminar el bucle

