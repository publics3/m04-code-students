# Ejercicio 13 - Cifrado César

Implementa el legendario cifrado César:

> En criptografía, un cifrado César, también conocido como cifrado de César, cifrado por desplazamiento, código de César o desplazamiento de César, es una de las técnicas de cifrado más simples y ampliamente conocidas. Es un tipo de cifrado de sustitución en el que cada letra en el texto plano es reemplazada por una letra ubicada fija número de posiciones más abajo en el alfabeto. Por ejemplo, con un desplazamiento hacia la izquierda de 3, la letra D sería reemplazada por A, E se convertiría en B, y así sucesivamente. El método lleva el nombre de Julio César, quien lo utilizó en su correspondencia privada.

Pista: Puede que necesites convertir las letras a sus valores Unicode. ¡Asegúrate de leer la documentación!

Escribe una función que tome una cadena para ser codificada y un factor de desplazamiento, y luego devuelva la cadena codificada:


```javascript
caesar('A', 1) // simplemente desplaza la letra en 1: devuelve 'B'
```

the cipher should retain capitalization:
```javascript
caesar('Hey', 5) // returns 'Mjd'
```

should _not_ shift punctuation:
```javascript
caesar('Hello, World!', 5) //returns 'Mjqqt, Btwqi!'
```

the shift should wrap around the alphabet:
```javascript
caesar('Z', 1) // returns 'A'
```

negative numbers should work as well:
```javascript
caesar('Mjqqt, Btwqi!', -5) // returns 'Hello, World!'
```
