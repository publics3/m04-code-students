# Ejercicio 09 - Palíndromos

Escribe una función que determine si una cadena dada es un palíndromo.

Un palíndromo es una cadena que se lee de la misma manera hacia adelante y hacia atrás, generalmente sin tener en cuenta puntuación o espacios entre palabras:

## Algunos palíndromos:
  - A car, a man, a maraca.
  - Rats live on no evil star.
  - Lid off a daffodil.
  - Animal loots foliated detail of stool lamina.
  - A nut for a jar of tuna.


```javascript
palindromes('racecar') // true
palindromes('tacos') // false
```


