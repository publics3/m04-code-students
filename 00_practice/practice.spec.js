const { numero, texto, esVerdadero } = require('./1_variables');
const { sumar, restar, multiplicar, dividir } = require('./2_operadores');
const { identificarNumero } = require('./3_condicionales');
const { esPalindromo, sumaNumerosPares } = require('./4_bucles');
const { encontrarMaximo, invertirArray, calcularSuma } = require('./5_arrays');


describe('Practice', () => {
  beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation(() => {});
  });

  afterEach(() => {
    console.log.mockRestore();
  });

  /*
  * VARIABLES (#1)
  */
  test('verificar declaraciones y tipos de variables', () => {
    // Verificar si las variables fueron declaradas
    expect(typeof numero).toBeDefined();
    expect(typeof texto).toBeDefined();
    expect(typeof esVerdadero).toBeDefined();

    // Verificar tipos de datos
    expect(typeof numero).toBe('number');
    expect(typeof texto).toBe('string');
    expect(typeof esVerdadero).toBe('boolean');
  });

  test('verificar la concatenación de Strings', () => {
    // Verificar concatenación
    expect(texto + numero).toBe('Hola, mundo!5');
  });

  /*
  * OPERADORES (#2)
  */
  test.skip('suma', () => {
    expect(sumar(10, 5)).toBe(15);
  });

  test.skip('resta', () => {
    expect(restar(10, 5)).toBe(5);
  });

  test.skip('multiplicación', () => {
    expect(multiplicar(10, 5)).toBe(50);
  });

  test.skip('división', () => {
    expect(dividir(10, 5)).toBe(2);
  });

  test.skip('maneja la división por cero', () => {
    expect(dividir(10, 0)).toBe('No se puede dividir por cero.');
  });

  /*
  * CONDICIONALES (#3)
  */
  test.skip('identificarNumero - Positivo', () => {
    expect(identificarNumero(5)).toBe('El número es positivo');
  });

  test.skip('identificarNumero - Negativo', () => {
    expect(identificarNumero(-3)).toBe('El número es negativo');
  });

  test.skip('identificarNumero - Cero', () => {
    expect(identificarNumero(0)).toBe('El número es cero');
  });

  /*
  * BUCLES (#4)
  */
  test.skip('esPalindromo - Palíndromo', () => {
    expect(esPalindromo("reconocer")).toBe(true);
  });

  test.skip('esPalindromo - No Palíndromo', () => {
    expect(esPalindromo("javascript")).toBe(false);
  });

  test.skip('sumaNumerosPares - Números Pares hasta 8', () => {
    expect(sumaNumerosPares(8)).toBe(20);
  });

  test.skip('sumaNumerosPares - Números Pares hasta 10', () => {
    expect(sumaNumerosPares(10)).toBe(30);
  });


  /*
  * ARRAYS (#5)
  */
  test('encontrarMaximo - Números Positivos', () => {
    expect(encontrarMaximo([3, 7, 1, 10, 5])).toBe(10);
  });
  
  test('encontrarMaximo - Números Negativos', () => {
    expect(encontrarMaximo([-2, -8, -1, -5])).toBe(-1);
  });
  
  // Pruebas para invertirArray
  test('invertirArray - Números Enteros', () => {
    expect(invertirArray([1, 2, 3, 4])).toEqual([4, 3, 2, 1]);
  });
  
  test('invertirArray - Caracteres', () => {
    expect(invertirArray(['a', 'b', 'c'])).toEqual(['c', 'b', 'a']);
  });
  
  // Pruebas para calcularSuma
  test('calcularSuma - Números Enteros', () => {
    expect(calcularSuma([1, 2, 3, 4])).toBe(10);
  });
  
  test('calcularSuma - Números Negativos', () => {
    expect(calcularSuma([-5, 10, 2])).toBe(7);
  });
});
