# Ejercicio 11 - ¡Obtén los Títulos!

Se te proporciona un array de objetos que representan libros con un autor y un título que se ve así:


```javascript
const books = [
  {
    title: 'Book',
    author: 'Name'
  },
  {
    title: 'Book2',
    author: 'Name2'
  }
]
```

Tu tarea es escribir una función que tome el array y devuelva un array de títulos:

```javascript
getTheTitles(books) // ['Book','Book2']
```

## Consejos

- Deberías usar un método incorporado de JavaScript para hacer la mayor parte del trabajo por ti.
